<div align="center">
  <h1>Composite</h1>
</div>

<div align="center">
  <img src="composite_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Composite is a structural pattern for composing objects into a tree structures and treating
them uniformly.**

### Real-World Analogy

_A physical filing system._

You can organize your documents (leafs) into folders (composite), which themselves can be in folders too.

### Participants

- :bust_in_silhouette:/:man: **Component**
    - Defines abstract methods for:
        - `operation` methods required for objects in the composition
    - Provides implementation for:
        - `addChild`
        - `removeChild`
        - `getChild`

- :man: **Leaf**: A leaf has no children
    - Provides implementation for:
        - `operation`

- :man: **Composite**: Defines behavior for components that have children.
    - stores child components
    - Provides implementation for:
        - `operation`

### Collaborations

Clients use the **Component** class interface to interact with objects in the composite structure. If the recipient is a
**Leaf**, then the request is handled directly. If the recipient is a **Composite**, then it usually forwards requests
to its child components, possibly performing additional operations before and/or after forwarding.

<br>
<br>

## When do you use it?

> :large_blue_diamond: **To represent object hierarchies, or to enable treating composite and leaf nodes uniformly.**

### Motivation

- How do we implement tree-like object structures?
- How do we prevent the client code from having to make the distinction between container (composite) and primitive
  (leaf) nodes in an object hierarchy?

### Known Uses

- Financial domain:
    - A portfolio can aggregate individual assets. You can support complex aggregations of assets by implementing a
      portfolio as a Composite.
- Billing and Invoicing Systems:
    - Represent invoices with line items, where line items can be individual charges or sub-invoices.
- Discounts:
    - Handling single discounts and multiple (chained) discounts uniformly.
- Organization Structure:
    - Operations that apply to both individual employees and departments can be uniformly applied.
- Playlists:
    - Music playlists often contain individual songs or nested playlists within them. The Composite pattern allows
      operations like playing, shuffling, or adding songs to be applied uniformly to individual tracks or entire
      playlists.
- Project Management Tools:
    - Project structures often have tasks, subtasks, and milestones. The Composite pattern can be used to represent
      these project structures, enabling uniform operations for individual tasks or entire project phases.
- UI Components and Structures:
    - User interfaces often consist of complex object hierarchies (windows, panels, buttons). Using the composite
      pattern, each component can be treated uniformly regardless of whether it's an individual element or a composite.
- Web Page Layouts:
    - HTML elements can be nested within other elements, creating complex web page layouts. CSS styling rules can be
      applied to individual elements or groups of elements.
- File Permissions:
    - File systems often have files and directories with permissions. The Composite pattern can be used to represent
      these permissions hierarchically, allowing operations like setting permissions at different levels of
      the file system.
- Directory structure:
    - Directories and files can be represented as a tree structure using the Composite pattern. Operations like copying
      or moving can be applied uniformly to both individual files and directories.

### Categorization

Purpose:  **Structural**  
Scope:    **Object**   
Mechanisms: **Polymorphism**, **Composition**

Structural patterns are concerned with how classes and objects are composed to form larger structures.

Structural object patterns describe ways to compose objects to realize new functionality.

### Aspects that can vary

- Structure and composition of an object.

### Solution to causes of redesign

- Extending functionality by subclassing.
    - Hard to understand: comprehending a subclass requires in-depth knowledge of all parent classes.
    - Hard to change: a modification to a parent might break multiple children classes.
    - Hard to reuse partially: all class members are inherited, even when not applicable, which can lead to a class
      explosion.

### Consequences

| Advantages                                                                                                                                                                                  | Disadvantages                                                                                                                                                                                        |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Simplifies client code.** <br> Clients can treat composite structures and leaf objects uniformly. Avoids writing tag-and-case-statement-style functions in the client. | :x: **Can make design overly general.** <br> Sometimes you want a composite to have only certain components. With Composite, you can't rely on the type system to enforce those constraints for you. |
| :heavy_check_mark: **Easily add new component types.** <br> Newly defined Composite or Leaf subclasses work automatically with existing structures and client code.                         | :x: **Can be difficult to provide a common interface.** <br> If the classes differ too much in functionality, you’d need to overgeneralize the component interface, making it harder to comprehend.  |

### Relations with Other Patterns

_Distinction from other patterns:_

- Visitor localizes operations and behavior that would otherwise be distributed across Composite and Leaf classes
- Composite and Decorator have similar structure diagrams since both rely on recursive composition to organize an
  open-ended number of objects.
    - A Decorator is like a Composite but only has one child component.
    - Decorator adds additional responsibilities to the wrapped object
    - Composite just “sums up” its children’s results.

_Combination with other patterns:_

- Chain of Responsibility is often used in conjunction with Composite. In this case, when a leaf component gets a
  request, it may pass it through the chain of all the parent components down to the root of the object tree.
- Decorator is often used with Composite.
    - When decorators and composites are used together, they will usually have a common parent class. So decorators will
      have to support the Component interface with operations like Add, Remove, and GetChild.
    - The Decorator can extend the behavior of a specific object in the Composite tree.
- You can implement shared leaf nodes of the Composite tree as Flyweights to save some RAM. But they can no longer refer
  to their parents.
- Iterator can be used to traverse composites.
- You can use Builder when creating complex Composite trees because you can program its construction steps to work
  recursively.
- You can use Visitor to execute an operation over an entire Composite tree.
- Designs that make heavy use of Composite and Decorator can often benefit from using Prototype. Applying the pattern
  lets you clone complex structures instead of re-constructing them from scratch.

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **An abstract Component class for managing children, extended by Composite and Leaf classes. The
Composite often delegates work to its Leafs.**

### Structure

```mermaid
classDiagram
    class Component {
        <<abstract>>
        - children: Component[]
        + operation(): void*
        + addChild(component: Component): void
        + removeChild(component: Component): void
        + getChild(index: int): Component
    }

    class Leaf {
        + operation(): void
    }

    class Composite {
        + operation(): void
    }

    Component <|.. Leaf: implements
    Component <|.. Composite: implements
    Component o-- Component: aggregated by
```

### Variations

_Explicit parent references:_

- **Only child references**
    - :heavy_check_mark: Simpler to keep consistent.
    - :x: Harder to find component parent nodes.
- **Store parent references**: usually defined in the Component class.
    - :heavy_check_mark: Simplifies moving up the structure and deleting a component.
    - :heavy_check_mark: Help support the Chain of Responsibility pattern.
    - :x: Requires additional code to maintain consistency. When A adds B as a child, B automatically needs to set A as
      its parent.

_Declaring the child management operations:_

- **Implement in Component**
    - :heavy_check_mark: Provides transparency, because all components can be treated uniformly.
    - :heavy_check_mark: With creativity, some methods can make sense for leaf nodes too. If we view a Leaf as a
      component without children, then the getChild method can just return None.
    - :x: The cost is safety, because clients may try meaningless operations like adding children to leaf nodes.
- **Implement in Composite**
    - :heavy_check_mark: Provides safety, because adding or removing child nodes from a leaf node will be caught at
      compile time.
    - :x: The cost is transparency, because leaves and composites have different interface

_Organization and optimization_

- **Child ordering:** for example, in graphics, child order may reflect front-to-back ordering.
    - :x: Have to design child access and management interfaces carefully to manage the sequence of children.
- **Caching:** cache search results or info that lets it short-circuit the traversal (e.g. picture class caches bounding
  box of children.)
    - :heavy_check_mark: Improves performance.
    - :x: Changes to a component will require invalidating the caches of its parents.
- **Sharing components:** using Flyweights
    - :heavy_check_mark: Reduces memory consumption.
    - :x: only works in cases where children can avoid sending parent requests.

### Implementation

In this example, we apply the composite pattern to a webshop that allows combining different types of discount on
products.

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Composite](https://refactoring.guru/design-patterns/composite)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [ZoranHorvat: Master the Power of Composite Pattern in Rich Domain Modeling](https://youtu.be/1l_hHoMgTV0?si=EdEzdmHgCGzw6dNq)
- [ZoranHorvat: Combine Design Patterns To Reveal Their Greatest Power](https://youtu.be/b1JyDzCe9n8?si=GAPwckCNdZzzo_Zo)
- [Geekific: The Composite Pattern Explained and Implemented in Java](https://youtu.be/oo9AsGqnisk?si=noSB607thwPPujJE)
- [pentalog: composite-design-pattern](https://www.pentalog.com/blog/design-patterns/composite-design-pattern/)

<br>
<br>
